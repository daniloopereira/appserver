package br.com.app.http.rest.api;

import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.app.core.repository.entity.User;

@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public interface UserRestAPI {

	@GET
	String helloUser();
	
	@GET
	@Path("/list")
	@RolesAllowed("admin")
	Response list();
	
	@PUT
	@Path("/create")
	Response create(String userJson);
	
	@POST
	@Path("/update")
	Response update(User user);
	
	@POST
	@Path("/delete")
	Response delete(String phone);
	
	@GET
	@Path("/findByPhone")
	Response findByPhone(@QueryParam("phones") List<String> phones);
	
	@GET
	@Path("/findByName")
	Response findByName(@QueryParam("name") String name);
	
	@GET
	@Path("/login")
	Response login(@QueryParam("username")String username, @QueryParam("password") String password);
	
	
}
