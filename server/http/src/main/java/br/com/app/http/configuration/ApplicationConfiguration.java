package br.com.app.http.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.ResourceBundleViewResolver;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

@Configuration
@EnableWebMvc
@Import({ SecurityConfiguration.class })
@PropertySource("classpath:app.properties")
@ComponentScan(basePackages = { "br.com.app.http" }, excludeFilters = { @Filter(Configuration.class) })
public class ApplicationConfiguration extends WebMvcConfigurerAdapter {

	public enum ConfigProps {
		REST_PATH("contextPath.rest"), WEB_PATH("contextPath.web"), REPORT_IMAGES(
				"report.images");

		private final String propertyKey;

		private ConfigProps(String propertyKey) {
			this.propertyKey = propertyKey;
		}

		public String value(Environment env) {
			return env.getProperty(propertyKey);
		}
	}

	@Override
	public void configureDefaultServletHandling(
			DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}

	@Bean
	public ResourceBundleMessageSource messageSource() {
		final ResourceBundleMessageSource bundleMessageSource = new ResourceBundleMessageSource();
		bundleMessageSource.setBasename("messages");
		return bundleMessageSource;
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return NoOpPasswordEncoder.getInstance();
	}

	@Bean
	public UrlBasedViewResolver setupViewResolver() {
		final UrlBasedViewResolver resolver = new UrlBasedViewResolver();
		resolver.setPrefix("/WEB-INF/pages/");
		resolver.setSuffix(".jsp");
		resolver.setViewClass(JstlView.class);
		resolver.setOrder(1);
		return resolver;
	}

	@Bean
	public CommonsMultipartResolver multipartResolver() {
		return new CommonsMultipartResolver();
	}

	// Jasper Reports COnfig
	// ---------------------------------------------------------------------------
	@Bean
	public ResourceBundleViewResolver viewResolver() {
		final ResourceBundleViewResolver bundleViewResolver = new ResourceBundleViewResolver();
		bundleViewResolver.setBasename("views");
		bundleViewResolver.setOrder(0);

		return bundleViewResolver;
	}

	// MVC configurations ------------------------------------------
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations(
				"/resources/");
	}

}