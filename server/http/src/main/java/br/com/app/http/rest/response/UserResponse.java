package br.com.app.http.rest.response;

import java.util.List;

import br.com.app.core.repository.model.UserVO;
import br.com.app.http.rest.util.ResponseStatusMessage;

public class UserResponse extends ResponseStatusMessage{

	private static final long serialVersionUID = 1L;
	
	private List<UserVO> users;
	private UserVO user;
	
	public UserResponse(UserVO userVO){
		this.user = userVO;
	}
	
	public UserResponse(List<UserVO> usersVO){
		this.users = usersVO;
	}

	public List<UserVO> getUsers() {
		return users;
	}

	public void setUsers(List<UserVO> users) {
		this.users = users;
	}

	public UserVO getUser() {
		return user;
	}

	public void setUser(UserVO user) {
		this.user = user;
	}
	
	

}
