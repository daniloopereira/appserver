package br.com.app.http.rest.impl;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.app.core.facade.api.UserFacade;
import br.com.app.core.repository.entity.User;
import br.com.app.core.repository.model.UserVO;
import br.com.app.http.rest.api.UserRestAPI;
import br.com.app.http.rest.response.UserResponse;
import br.com.app.http.rest.util.ResponseStatusMessage;

import com.google.gson.Gson;

@Component
public class UserRestAPIImpl implements UserRestAPI {
	
	@Autowired
	private UserFacade userFacade;

	@Override
	public String helloUser() {
		return "{\"user\": \"Hello me\"}";
	}

	@Override
	public Response list() {
		List<UserVO> list = userFacade.findAll();
		if(list ==  null || list.isEmpty()){
			return Response.ok(ResponseStatusMessage.USERS_LIST_EMPTY).build();
		}
		
		return Response.ok(new UserResponse(list)).build();
	}

	@Override
	public Response create(String userJson) {
		if(userJson == null){
			return Response.ok(ResponseStatusMessage.USER_NULL).build();
		}
		
		Gson gson = new Gson();
		User user = gson.fromJson(userJson, User.class);
		
		
		user.setCreateDate(DateTime.now());
		
		user.setId(userFacade.createUser(user));
		
		UserVO userVO = new UserVO(user);		
		
		return Response.ok(new UserResponse(userVO)).build();
	}

	@Override
	public Response update(User user) {
		User userResponse = userFacade.findByPhone(user.getPhone());
		if(userResponse == null){
			return Response.ok(ResponseStatusMessage.USER_NOT_FOUND).build();
		}
		
		userResponse = userFacade.editUser(user);
		
		return Response.ok(userResponse).build();
	}

	@Override
	public Response delete(String phone) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Response findByPhone(List<String> phones) {
		if(phones.isEmpty() || phones == null){
			return Response.ok(ResponseStatusMessage.USERS_LIST_EMPTY).build();
		}
		List<User> users = new ArrayList<User>();
		for(String phone: phones){
			users.add(userFacade.findByPhone(phone));
		}
		
		return null;
	}

	@Override
	public Response findByName(String name) {
		if(name == null){
			
		}
		return null;
	}

	@Override
	public Response login(String username, String password) {
		// TODO Auto-generated method stub
		return null;
	}
}