package br.com.app.http.configuration;

import java.util.Locale;

import javax.servlet.FilterRegistration;
import javax.servlet.FilterRegistration.Dynamic;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;

import br.com.app.core.repository.config.RepositoryConfiguration;
import br.com.app.http.configuration.ApplicationConfiguration.ConfigProps;
import br.com.app.http.filter.SystemRestFilter;
import br.com.app.http.web.session.SessionListener;

import com.sun.jersey.spi.spring.container.servlet.SpringServlet;

public class WebAppInitializer implements WebApplicationInitializer {

	static {
		System.setProperty("jsse.enableSNIExtension", "false");

		Locale.setDefault(new Locale("pt-BR"));
	}

	@Override
	public void onStartup(final ServletContext servletContext)
			throws ServletException {

		AnnotationConfigWebApplicationContext root = null;

		// Bootstrap
		root = bootstrapSpring(servletContext);

		// Application Listeners (Spring, Logging, etc)
		registerListeners(servletContext, root);

		// Application Filters
		registerFilters(servletContext, root);

		// Application Servlets
		registerServlets(servletContext, root);
	}

	private void registerServlets(ServletContext servletContext,
			AnnotationConfigWebApplicationContext root) {
		// Jersey --------------
		final ServletRegistration.Dynamic jerseyServlet = servletContext
				.addServlet(SpringServlet.class.getName(), SpringServlet.class);
		jerseyServlet.setLoadOnStartup(0);
		jerseyServlet.addMapping(ConfigProps.REST_PATH.value(root
				.getEnvironment()));
		jerseyServlet.setInitParameter(
				"com.sun.jersey.api.json.POJOMappingFeature",
				Boolean.TRUE.toString());
		jerseyServlet.setAsyncSupported(true);
	}

	private void registerListeners(ServletContext servletContext,
			AnnotationConfigWebApplicationContext root) {

		servletContext.addListener(new LoggingInitializerListener());
		servletContext.addListener(new ContextLoaderListener(root));
		servletContext.addListener(new RequestContextListener());
		servletContext.addListener(new HttpSessionEventPublisher());
		servletContext.addListener(new SessionListener());
	}
	
	private void registerFilters(ServletContext servletContext,
			AnnotationConfigWebApplicationContext root) {

		final Dynamic dynamic = servletContext.addFilter(
				"springSecurityFilterChain", new DelegatingFilterProxy(
						"springSecurityFilterChain", root));
		dynamic.addMappingForUrlPatterns(null, false, "/*");
		dynamic.setAsyncSupported(true);

		// Jersey --------------
		final FilterRegistration.Dynamic jerseyServlet = servletContext
				.addFilter(SystemRestFilter.class.getName(),
						SystemRestFilter.class);
		jerseyServlet
				.addMappingForUrlPatterns(null, false,
						ConfigProps.REST_PATH.value(root
								.getEnvironment()));
		jerseyServlet.setAsyncSupported(true);
	}

	// Initializes a Configurable Bean Context
	private AnnotationConfigWebApplicationContext bootstrapSpring(
			ServletContext servletContext) {

		final AnnotationConfigWebApplicationContext root = new AnnotationConfigWebApplicationContext();
		root.setServletContext(servletContext); // registers the servlet context
		root.register(ApplicationConfiguration.class,
				RepositoryConfiguration.class);
		root.refresh(); // refreshes all beans
		
		return root;
	}
}