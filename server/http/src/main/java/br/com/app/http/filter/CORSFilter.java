package br.com.app.http.filter;


import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

public class CORSFilter extends BasicAuthenticationFilter {

	private static final Logger LOG = LoggerFactory.getLogger(CORSFilter.class);

	public CORSFilter(AuthenticationManager authenticationManager) {
		super(authenticationManager);
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		LOG.info("attemptAuthentication({},{})", request, response);

		final HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		final HttpServletResponse httpServletResponse = (HttpServletResponse) response;

		httpServletResponse.setCharacterEncoding("UTF-8");

		httpServletResponse.setHeader("Access-Control-Allow-Origin",
				httpServletRequest.getHeader("Origin"));
		httpServletResponse.setHeader("Access-Control-Allow-Methods",
				"POST, GET, PUT, OPTIONS");
		httpServletResponse.setHeader("Access-Control-Max-Age", "3600");
		httpServletResponse
				.setHeader("Access-Control-Allow-Headers",
						"Accept, Origin, X-Requested-With, Content-Type, Last-Modified, Authorization");
		httpServletResponse.addHeader("Access-Control-Allow-Credentials",
				"true");
		httpServletResponse.addHeader("Content-Type", "application/json");

		if (httpServletRequest.getMethod().equals("OPTIONS")) {
			httpServletResponse.setStatus(200);
			httpServletResponse.getWriter().write("OK");

			return;
		}

		super.doFilter(request, response, chain);
	}

	@Override
	public void destroy() {

	}

}
