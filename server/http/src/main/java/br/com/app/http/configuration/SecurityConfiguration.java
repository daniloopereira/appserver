package br.com.app.http.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;

import br.com.app.core.facade.api.ClientAuthFacade;
import br.com.app.http.filter.CORSFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration{

	@Order(1)
	@Configuration
	public static class RestSecurityConfiguration extends
			WebSecurityConfigurerAdapter {

		@Autowired
		private ClientAuthFacade clientAuthFacade;

		@Autowired
		@Qualifier("authenticationManagerRest")
		private AuthenticationManager authenticationManager;

		@Autowired
		private PasswordEncoder passwordEncoder;

		@Override
		public void configure(AuthenticationManagerBuilder auth)
				throws Exception {

			// Real User
			auth.userDetailsService(clientAuthFacade)
					.passwordEncoder(passwordEncoder);
		}

		@Bean(name = "authenticationManagerRest")
		@Override
		public AuthenticationManager authenticationManagerBean()
				throws Exception {
			return super.authenticationManagerBean();
		}

		@Override
		protected void configure(HttpSecurity http) throws Exception {

			http.antMatcher("/rest/**")
					.addFilterAfter(new CORSFilter(authenticationManager),
							SecurityContextPersistenceFilter.class)
					// URLS
					.authorizeRequests()
					.antMatchers("/rest/application.wadl")
					.permitAll()
					// API
					.antMatchers("/rest/***").hasAnyRole("ADMIN")
					.anyRequest().authenticated()
					// Auth
					.and().httpBasic()
					// disable CSRF
					.and().csrf().disable().headers().frameOptions().disable()
					// Session Management
					.sessionManagement()
					.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		}
	}
}