package br.com.app.http.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.ModelAttribute;

import br.com.app.core.facade.api.UserFacade;

public abstract class BaseController {

	@Autowired
	private UserFacade userFacade;

	@ModelAttribute("user")
	public UserDetails getUser() {

		if (!SecurityContextHolder.getContext().getAuthentication()
				.isAuthenticated())
			return null;

		final Object principal = SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

//		if (principal instanceof User)
//			return (User) SecurityContextHolder.getContext()
//					.getAuthentication().getPrincipal();
//		else if (principal instanceof org.springframework.security.core.userdetails.User)
//			return (org.springframework.security.core.userdetails.User) SecurityContextHolder
//					.getContext().getAuthentication().getPrincipal();

		return null;
	}

	public UserFacade getUserFacade() {
		return userFacade;
	}
	
}
