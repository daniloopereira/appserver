package br.com.app.http.rest.util;

import java.io.Serializable;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public class ResponseStatusMessage implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Integer code = 100;
	
	private String message = "OK";
	
	private transient String baseMessage = "OK";
	
	public transient static final ResponseStatusMessage OK = new ResponseStatusMessage();
	
	public transient static final ResponseStatusMessage USER_NULL = new ResponseStatusMessage(
			101, "User is null");

	public transient static final ResponseStatusMessage USER_NOT_FOUND = new ResponseStatusMessage(
			102, "User not found");
	
	public transient static final ResponseStatusMessage USERS_LIST_EMPTY = new ResponseStatusMessage(
			103, "Users list is empty");

	
	protected ResponseStatusMessage() {
	}
	
	private ResponseStatusMessage(Integer code, String message){
		this.code = code;
		this.message = message;
		this.baseMessage = message;
	}
	
	public ResponseStatusMessage format(Object... args) {

		if (args == null)
			return this;

		this.message = String.format(baseMessage, args);

		return this;
	}

	public String generateJSON() throws JsonProcessingException {
		final ObjectWriter ow = new ObjectMapper().writer()
				.withDefaultPrettyPrinter();
		
		

		return ow.writeValueAsString(this);
	}

	public Integer getCode() {
		return code;
	}

	public String getMessage() {
		return message;
	}
	
	
	

}
