package br.com.app.http.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class IndexController extends BaseController {

	private static final Logger LOG = LoggerFactory
			.getLogger(IndexController.class);

	@RequestMapping(method = RequestMethod.GET)
	public String index(Model model) {

		LOG.info("Calling index()");

		return "index";
	}
}
