package br.com.app.http.web.session;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class SessionListener implements HttpSessionListener {

	@Override
	public void sessionCreated(HttpSessionEvent arg0) {
		System.out.println("==== Session is created ====");
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent arg0) {
		System.out.println("==== Session is destroyed ====");
	}

}
