<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<t:main >
	<c:if test="${not empty error}">
		<br />
		<div class="alert alert-danger">
			${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
		</div>
	</c:if>

	<div class="row" align="center">
		<div class="col-xs-12 col-sm-12 col-md-5 col-lg-4" style="float: none">
			<div class="well no-padding">
				<form action="<c:url value='login' />" id="login-form" class="smart-form client-form" method="post">
					<header> Login </header>

					<fieldset>
						<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
						<section>
							<label class="label">Usuário</label> <label class="input"> <i class="icon-append fa fa-user"></i> <input
									type="text" name="username"> <b class="tooltip tooltip-top-right"><i
										class="fa fa-user txt-color-teal"></i> Preencha com o usuário</b></label>
						</section>

						<section>
							<label class="label">Senha</label> <label class="input"> <i class="icon-append fa fa-lock"></i> <input
									type="password" name="password"> <b class="tooltip tooltip-top-right"><i
										class="fa fa-lock txt-color-teal"></i> Preencha com o password</b>
							</label>
						</section>
						
					</fieldset>
					<footer>
						<button type="submit" class="btn btn-primary">Entrar</button>
					</footer>
				</form>

			</div>
		</div>
	</div>
</t:main>