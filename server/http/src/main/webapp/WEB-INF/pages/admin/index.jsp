<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<t:main>

	<sec:authorize access="hasRole('ROLE_ADMIN')">
		<div class="panel panel-default">
			<!-- 			Default panel contents -->
			<div class="panel-heading">Status executores de Captcha</div>
			<div class="panel-body">
				<table class="table table-hover table-striped table-bordered">
					<tbody>
						<tr>
							<th>Nome</th>
							<th>Está disponível?</th>
							<th>Balanço</th>
						</tr>

						<c:forEach var="executor" items="${captchaExecutors}"
							varStatus="i">
							<tr>
								<td style="text-align: left; width: 40%;">${executor.class.simpleName}</td>
								<td style="text-align: left; width: 30%;">${executor.available}</td>
								<td style="text-align: left; width: 30%;">${executor.balance}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>

		<div class="panel panel-default">
			<!-- Default panel contents -->
			<div class="panel-heading">Instâncias ligadas</div>
			<div class="panel-body">${instancesOn}</div>
		</div>

		<div class="panel panel-default">
			<!-- Default panel contents -->
			<div class="panel-heading">Instâncias desligadas</div>
			<div class="panel-body">${instancesOff}</div>
		</div>
	</sec:authorize>

	<sec:authorize access="hasRole('ROLE_USER')">

		<div class="well">
			<h2>
				Olá
				<sec:authentication property="principal.username" />
				. Bem vindo ao <span id="logo"> <img src="/resources/img/logo.png" alt="SISCONSIGNA">!
			</h2>
			<p>Navegue pelo menu ao lado para:
			<ul>
				<li><strong>Executar uma operação</strong>, selecione: Fluxo ->
					Execução</li>
				<li><strong>Importar um arquivo de dados</strong>, selecione: Fluxo -> Importação de dados</li>
				<li><strong>Gerar relatórios</strong>, selecione: Relatórios -> Resultados</li>
				<li><a href="/web/user/changePassword" class="btn btn-primary btn-xs">Clique aqui</a> para trocar a senha do usuário</li>
			</ul>
		</div>
	</sec:authorize>

</t:main>