package br.com.app.http.test;

import org.hibernate.SessionFactory;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import br.com.app.core.repository.config.BeanValidationConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { TestRepositoryConfiguration.class,
		BeanValidationConfiguration.class }, loader = AnnotationConfigContextLoader.class)
@Ignore
public abstract class RepositoryBaseTest {

	@Autowired
	private SessionFactory sessionFactory;
}
