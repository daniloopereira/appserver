package br.com.app.http.test;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.hibernate.SessionFactory;
import org.hibernate.dialect.H2Dialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import br.com.app.core.repository.entity.User;

/**
 * Repository module configuration. It basically enables transaction management
 * on a standard JPA EntityManager session.
 * <p>
 * This configuration uses a properties file that must be place in the root
 * classpath.
 * 
 * @see Configuration
 * @see EnableTransactionManagement
 * @see PropertySource
 */
@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = { "br.com.paulistano.core" }, excludeFilters = { @Filter(Configuration.class) })
@PropertySource("classpath:jdbc.properties")
public class TestRepositoryConfiguration {

	@Autowired
	private Environment env;

	/**
	 * Configures an Apache Commons Basic Data Source according to the
	 * properties appointed down in jdbc.properties file
	 * 
	 * 
	 * @return the basic data source configured by the jdbc.properties file.
	 */
	@Bean
	@Primary
	public DataSource dataSource() {

		final BasicDataSource bds = new BasicDataSource();
		bds.setDriverClassName(env.getProperty("jdbc.driverClassName"));
		bds.setUrl(env.getProperty("jdbc.url"));
		bds.setUsername(env.getProperty("jdbc.username"));
		bds.setPassword(env.getProperty("jdbc.password"));
		bds.setPoolPreparedStatements(true);

		final Resource schema = new ClassPathResource("schema.sql");
		final Resource data = new ClassPathResource("data.sql");

		if (!schema.exists() && !data.exists())
			return bds;

		final ResourceDatabasePopulator rdp = new ResourceDatabasePopulator();

		if (schema.exists())
			rdp.addScript(schema);

		if (data.exists())
			rdp.addScript(data);

		DatabasePopulatorUtils.execute(rdp, bds);
		return bds;
	}

	/**
	 * Configures a local EntityManager Factory container using the properties
	 * set on jdbc.properties.
	 * 
	 * @return A local EntityManager Factory container
	 */
	@Bean
	@DependsOn("dataSource")
	public LocalSessionFactoryBuilder sessionFactoryBuilder(
			DataSource dataSource) {

		final Properties hibernateProperties = new Properties();
		hibernateProperties.put("hibernate.cache.region.factory_class",
				"org.hibernate.cache.ehcache.EhCacheRegionFactory");
		hibernateProperties.put("hibernate.cache.use_query_cache", "true");
		hibernateProperties.put("hibernate.cache.use_second_level_cache",
				"true");
		hibernateProperties.put("hibernate.generate_statistics", "true");
		hibernateProperties.put("hibernate.hbm2ddl.auto", "create-drop");
		hibernateProperties.put("hibernate.show_sql", "true");
		hibernateProperties.put("hibernate.format_sql", "true");
		hibernateProperties.put("jadira.usertype.currencyCode", "BRL");
		hibernateProperties.put("hibernate.dialect", H2Dialect.class.getName());
		hibernateProperties.put("hibernate.hbm2ddl.import_files_sql_extractor",
				"org.hibernate.tool.hbm2ddl.MultipleLinesSqlCommandExtractor");

		final LocalSessionFactoryBuilder sfBuilder = new LocalSessionFactoryBuilder(
				dataSource);

		sfBuilder.addProperties(hibernateProperties);

		return sfBuilder.scanPackages(User.class.getPackage().getName());
	}

	@Bean
	@DependsOn("sessionFactoryBuilder")
	public SessionFactory sessionFactory(
			LocalSessionFactoryBuilder localSessionFactoryBuilder) {
		return localSessionFactoryBuilder.buildSessionFactory();
	}

	@Bean
	@DependsOn("sessionFactory")
	public PlatformTransactionManager transactionManager(
			SessionFactory sessionFactory) {
		return new HibernateTransactionManager(sessionFactory);
	}
}