package br.com.app.core.test.repository.config;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;

import org.hibernate.SessionFactory;
import org.hibernate.dialect.MySQL5InnoDBDialect;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.Transactional;

import br.com.app.core.test.utils.RepositoryBaseTest;

public class ConfigTest extends RepositoryBaseTest {

	private static final Logger LOG = LoggerFactory.getLogger(ConfigTest.class);

	@Autowired
	private SessionFactory sessionFactory;

	@Autowired
	private LocalSessionFactoryBuilder builder;

	@Test
	@Transactional
	public void testConfig() {

		assertThat(sessionFactory, is(notNullValue()));
		assertThat(sessionFactory.getCurrentSession(), is(notNullValue()));
	}

	@Test
	public void testCreateSchema() {

		final String[] script = builder
				.generateSchemaCreationScript(new MySQL5InnoDBDialect());
		assertThat(script, is(notNullValue()));

		for (String sqlLine : script)
			LOG.info(sqlLine + ";");
	}
}
