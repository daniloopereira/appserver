package br.com.app.core.repository.model;

import br.com.app.core.repository.entity.User;
import br.com.app.core.repository.entity.api.EventStatus;
import br.com.app.core.repository.entity.api.UserSex;

public class UserVO implements Comparable<UserVO>{
	
	private Long id;
	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private String city;
	private UserSex sex;
	private String birthday;
	private String lastPosition;
	private EventStatus eventStatus;
	
	public UserVO(User user){
		this.id = user.getId();
		this.firstName = user.getFirstName();
		this.lastName = user.getLastName();
		this.email = user.getEmail();
		this.phone = user.getPhone();
		this.city = user.getCity();
		this.sex = user.getSex();
		this.birthday = user.getBirthday();
		this.eventStatus = user.getEventStatus();
		this.lastPosition = user.getLastPosition();
	}

	public String getLastPosition() {
		return lastPosition;
	}

	public void setLastPosition(String lastPosition) {
		this.lastPosition = lastPosition;
	}


	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public UserSex getSex() {
		return sex;
	}

	public void setSex(UserSex sex) {
		this.sex = sex;
	}
	
	public User toEntity(UserVO userVO){
		User user = new User();
		
		user.setId(userVO.getId());
		user.setFirstName(userVO.getFirstName());
		user.setLastName(userVO.getLastName());
		user.setBirthday(userVO.getBirthday());
		user.setCity(userVO.getCity());
		
		return user;
	}
	
	public UserVO(){
		
	}

	@Override
	public int compareTo(UserVO o) {
		// TODO Auto-generated method stub
		return 0;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public EventStatus getEventStatus() {
		return eventStatus;
	}

	public void setEventStatus(EventStatus eventStatus) {
		this.eventStatus = eventStatus;
	}

}
