package br.com.app.core.facade.api;

import java.util.List;

import org.joda.time.DateTime;

import br.com.app.core.repository.entity.Event;
import br.com.app.core.repository.entity.User;
import br.com.app.core.repository.entity.api.EventStatus;
import br.com.app.core.repository.entity.api.EventVisibility;

public interface EventFacade {
	
	Event create(Event event);
	
	void update(Event event);
	
	List<Event> findByName(String name);
	
	List<Event> findByDate(DateTime date);
	
	List<Event> findByAddress(String address);
	
	List<Event> findByDateAddress(String dateAddress);
	
	List<Event> findByStatus(EventVisibility status);
	
	List<User> findByUserStatus(Event event, EventStatus status);
	
	List<Event> findAll();

}
