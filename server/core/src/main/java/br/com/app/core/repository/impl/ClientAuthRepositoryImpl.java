package br.com.app.core.repository.impl;

import org.hibernate.Hibernate;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.app.core.repository.api.ClientAuthRepository;
import br.com.app.core.repository.entity.ClientAuth;

@Repository
public class ClientAuthRepositoryImpl extends GenericDAOImpl<ClientAuth, Long> implements ClientAuthRepository{

	@Override
	@Transactional(readOnly=true)
	public ClientAuth loadByUserName(String username) {
		final ClientAuth r = (ClientAuth) criteria().add(
				Restrictions.eq("username", username)).uniqueResult();

		if (r != null)
			Hibernate.initialize(r.getAuthorities());

		return r;
	}
}
