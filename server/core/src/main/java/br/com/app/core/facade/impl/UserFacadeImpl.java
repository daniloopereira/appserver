package br.com.app.core.facade.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.app.core.facade.api.UserFacade;
import br.com.app.core.repository.api.UserRepository;
import br.com.app.core.repository.entity.User;
import br.com.app.core.repository.model.UserVO;

@Component
public class UserFacadeImpl implements UserFacade {

	@Autowired
	private UserRepository userRepository;

	@Override
	public List<UserVO> findAll() {
		List<User> list = userRepository.findAll();
		List<UserVO> users = new ArrayList<UserVO>();
		
		for(User user : list){
			UserVO userVO = new UserVO();
			
			userVO.setId(user.getId());
			userVO.setBirthday(user.getBirthday());
			userVO.setFirstName(user.getFirstName());
			userVO.setLastName(user.getLastName());
			userVO.setSex(user.getSex());
			userVO.setCity(user.getCity());
			userVO.setLastPosition(user.getLastPosition());
			userVO.setEmail(user.getEmail());
			
			users.add(userVO);
		}
		
		return users;
	}

	@Override
	public User findByPhone(String phone) {
		return userRepository.findByPhone(phone);
	}

	@Override
	public List<User> findByName(String name) {
		return userRepository.findByName(name);
	}

	@Override
	public Long createUser(User user) {
		Long id = userRepository.create(user);
		return id;
	}

	@Override
	public User editUser(User user) {
		User userEdited = userRepository.update(user);
		return userEdited;
	}

	@Override
	public User login(String username, String password) {
		return userRepository.login(username, password);
	}

	
}
