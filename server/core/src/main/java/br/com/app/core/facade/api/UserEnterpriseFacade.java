package br.com.app.core.facade.api;

import java.util.List;

import br.com.app.core.repository.entity.UserEnterprise;

public interface UserEnterpriseFacade {
	
	List<UserEnterprise> findAll();
	
	void create(UserEnterprise userEnterprise);
	
	UserEnterprise update(UserEnterprise userEnterprise);
	
	List<UserEnterprise> findByName(String name);
	
	UserEnterprise findByPhone(String phone);
	
	UserEnterprise findByCnpj(String cnpj);
}
