package br.com.app.core.facade.api;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

import br.com.app.core.repository.entity.ClientAuth;
import br.com.app.core.repository.entity.api.Roles;

public interface ClientAuthFacade extends UserDetailsService{
	
	boolean isAdmin(final UserDetails authUser);
	
	void update(final ClientAuth cAuth);

	boolean isAutentication(String username, String password, PasswordEncoder passwordEncoder);

	boolean isAutenticationRoles(String username, String password, PasswordEncoder passwordEncoder, Roles roles);
	
	

}
