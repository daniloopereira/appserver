package br.com.app.core.facade.api;

import java.util.List;

import br.com.app.core.repository.entity.User;
import br.com.app.core.repository.model.UserVO;


public interface UserFacade {
	
	List<UserVO> findAll();
	
	User findByPhone(String phone);
	
	List<User> findByName(String name);
	
	Long createUser(User user);
	
	User editUser(User user);
	
	User login(String username, String password);
}
