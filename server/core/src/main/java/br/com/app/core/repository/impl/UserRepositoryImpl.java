package br.com.app.core.repository.impl;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.app.core.repository.api.UserRepository;
import br.com.app.core.repository.entity.User;

@Repository
public class UserRepositoryImpl extends GenericDAOImpl<User, Long> implements
UserRepository {

	@Override
	@Transactional(readOnly=true)
	public User findByPhone(String phone) {
		return (User) criteria().add(Restrictions.eq("phone", phone)).uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<User> findByName(String name) {
		return session().createQuery("SELECT * FROM User WHERE first_name like :searchKey").setParameter("searchKey", "%" + name +"%").list();
	}

	@Override
	public User login(String username, String password) {
		return (User) criteria().add(Restrictions.eq("email", username)).add(Restrictions.eq("password", password)).uniqueResult();
	}



}
