package br.com.app.core.repository.api;

import br.com.app.core.repository.entity.ClientAuth;

public interface ClientAuthRepository extends GenericDAO<ClientAuth, Long>{
	
	ClientAuth loadByUserName(String username);

}
