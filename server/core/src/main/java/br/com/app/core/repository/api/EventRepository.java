package br.com.app.core.repository.api;

import java.util.List;

import org.joda.time.DateTime;

import br.com.app.core.repository.entity.Event;
import br.com.app.core.repository.entity.User;
import br.com.app.core.repository.entity.api.EventStatus;
import br.com.app.core.repository.entity.api.EventVisibility;

public interface EventRepository extends GenericDAO<Event, Long>{
	
	List<Event> findByName(String name);
	
	List<Event> findByDate(DateTime date);
	
	List<Event> findByAddress(String address);
	
	List<Event> findByDateAddress(String dateAddress);
	
	List<Event> findByStatus(EventVisibility status);
	
	List<User> findByUserStatus(Event event, EventStatus status);
}
