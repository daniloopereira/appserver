package br.com.app.core.facade.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import br.com.app.core.facade.api.ClientAuthFacade;
import br.com.app.core.repository.api.ClientAuthRepository;
import br.com.app.core.repository.entity.ClientAuth;
import br.com.app.core.repository.entity.api.Roles;

@Component
public class ClientAuthFacadeImpl implements ClientAuthFacade{
	
	@Autowired
	ClientAuthRepository clientAuthRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		final ClientAuth clientAuth = clientAuthRepository.loadByUserName(username);
		
		if(clientAuth == null)
		return new ClientAuth();
		
		return clientAuth;
	}

	@Override
	public boolean isAdmin(UserDetails authUser) {
		for (GrantedAuthority authority : authUser.getAuthorities()) {
			if (authority.getAuthority().equals(Roles.ROLE_ADMIN.name()))
				return true;
		}

		return false;
	}

	@Override
	public void update(ClientAuth cAuth) {
		clientAuthRepository.update(cAuth);
	}
	
	@Override
	public boolean isAutentication(final String username,
			final String password, PasswordEncoder passwordEncoder) {

		boolean loginSuccessful = false;

		final UserDetails userDetails = this.loadUserByUsername(username);

		if (userDetails != null && userDetails.getPassword() != null)
			loginSuccessful = passwordEncoder.matches(password,
					userDetails.getPassword());


		return loginSuccessful;
	}

	@Override
	public boolean isAutenticationRoles(final String username,String password, PasswordEncoder passwordEncoder, Roles roles) {

		boolean loginSuccessful = false;

		final UserDetails userDetails = this.loadUserByUsername(username);

		if (userDetails != null && userDetails.getPassword() != null)
			for (GrantedAuthority authority : userDetails.getAuthorities()) {
				if (authority.getAuthority().equals(roles.name())) {

					loginSuccessful = passwordEncoder.matches(password,
							userDetails.getPassword());
					break;
				}
			}


		return loginSuccessful;
	}

}
