package br.com.app.core.repository.entity.api;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotNull;

import org.joda.time.DateTime;

@MappedSuperclass
public abstract class Auditable implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotNull
	@Column(name = "created_date", nullable = false)
	private DateTime createdDate = new DateTime();

	@NotNull
	@Column(name = "updated_date", nullable = false)
	private DateTime updatedDate = new DateTime();

	@Enumerated(EnumType.ORDINAL)
	@Column(name = "registry_status", nullable = false)
	private Activation registryStatus = Activation.ACTIVE;

	public static enum Activation {
		ACTIVE, INACTIVE;
	}

	public DateTime getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(DateTime createdDate) {
		this.createdDate = createdDate;
	}

	public DateTime getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(DateTime updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Activation getRegistryStatus() {
		return registryStatus;
	}

	public void setRegistryStatus(Activation registryStatus) {
		this.registryStatus = registryStatus;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((createdDate == null) ? 0 : createdDate.hashCode());
		result = prime * result
				+ ((registryStatus == null) ? 0 : registryStatus.hashCode());
		result = prime * result
				+ ((updatedDate == null) ? 0 : updatedDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Auditable))
			return false;
		Auditable other = (Auditable) obj;
		if (createdDate == null) {
			if (other.createdDate != null)
				return false;
		} else if (!createdDate.equals(other.createdDate))
			return false;
		if (registryStatus != other.registryStatus)
			return false;
		if (updatedDate == null) {
			if (other.updatedDate != null)
				return false;
		} else if (!updatedDate.equals(other.updatedDate))
			return false;
		return true;
	}

}
