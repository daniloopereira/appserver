package br.com.app.core.repository.entity.api;

import java.io.Serializable;
import java.util.Map;

public class GeoPosition implements Serializable{
	private static final long serialVersionUID = 1L;
	private Map<String, String> latitude;
	public Map<String, String> getLatitude() {
		return latitude;
	}
	public void setLatitude(Map<String, String> latitude) {
		this.latitude = latitude;
	}
	public Map<String, String> getLongitude() {
		return longitude;
	}
	public void setLongitude(Map<String, String> longitude) {
		this.longitude = longitude;
	}
	private Map<String, String> longitude;

}
