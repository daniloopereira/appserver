package br.com.app.core.repository.entity.api;

public enum EventVisibility {
	
	PUBLIC("public"), PRIVATE("private");
	
	private String message;
	
	private EventVisibility(String message){
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
	
	

}
