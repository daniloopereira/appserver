package br.com.app.core.repository.api;

import java.util.List;

import br.com.app.core.repository.entity.User;

public interface UserRepository extends GenericDAO<User, Long> {
	
	User findByPhone(String phone);
	
	List<User> findByName(String name);
	
	User login(String username, String password);
}
