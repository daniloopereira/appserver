package br.com.app.core.repository.config;

import java.util.Properties;

import javax.naming.NamingException;
import javax.sql.DataSource;

import org.flywaydb.core.Flyway;
import org.hibernate.SessionFactory;
import org.hibernate.dialect.MySQL5Dialect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jndi.JndiTemplate;
import org.springframework.orm.hibernate4.HibernateTransactionManager;
import org.springframework.orm.hibernate4.LocalSessionFactoryBuilder;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import br.com.app.core.repository.entity.User;

/**
 * Repository module configuration. It basically enables transaction management
 * on a standard JPA EntityManager session.
 * <p>
 * This configuration uses a properties file that must be place in the root
 * classpath.
 * 
 * @see Configuration
 * @see EnableTransactionManagement
 * @see PropertySource
 */
@Configuration
@EnableTransactionManagement
@ComponentScan(basePackages = { "br.com.app.core" }, excludeFilters = { @Filter(Configuration.class) })
@Import(BeanValidationConfiguration.class)
@PropertySource("classpath:datasource.properties")
public class RepositoryConfiguration extends JndiTemplate {
	
	private static final String DATASOURCE_KEY = "datasource.name";

	@Autowired
	private Environment env;
	
	@Bean(destroyMethod = "")
	public DataSource dataSource() {

		try {
			final DataSource lookup = lookup(env.getProperty(DATASOURCE_KEY),
					DataSource.class);

			final Flyway flyway = new Flyway();
			flyway.setDataSource(lookup);
			flyway.setInitOnMigrate(true);
			flyway.setValidateOnMigrate(true);
			flyway.setSchemas("appserver");
			flyway.migrate();

			return lookup;

		} catch (NamingException e) {

			throw new RuntimeException(
					"Error while trying to fetch the DataSource through JNDI.");
		}
	}

	@Bean
	@Autowired
	public SessionFactory sessionFactory(DataSource dataSource) {

		final Properties hibernateProperties = new Properties();
		hibernateProperties.put("hibernate.cache.region.factory_class",
				"org.hibernate.cache.ehcache.SingletonEhCacheRegionFactory");
		hibernateProperties.put("hibernate.cache.use_query_cache", "true");
		hibernateProperties.put("hibernate.cache.use_second_level_cache",
				"true");

		hibernateProperties.put("hibernate.c3p0.min_size", "5");
		hibernateProperties.put("hibernate.c3p0.max_size", "20");
		hibernateProperties.put("hibernate.c3p0.timeout", "3600");
		hibernateProperties.put("hibernate.c3p0.max_statements", "50");
		hibernateProperties.put("hibernate.c3p0.idle_test_period", "7200");
		hibernateProperties.put("hibernate.show_sql", "false");
		hibernateProperties.put("hibernate.dialect",
				MySQL5Dialect.class.getCanonicalName());
		hibernateProperties.put("hibernate.format_sql", "false");
		hibernateProperties.put(
				"org.jadira.usertype.dateandtime.joda.PersistentDateTime",
				"dateTime");

		// hibernateProperties.put("hibernate.hbm2ddl.auto", "create");

		final String modelPackage = User.class.getPackage().getName();

		final LocalSessionFactoryBuilder sfBuilder = new LocalSessionFactoryBuilder(
				dataSource);

		sfBuilder.addProperties(hibernateProperties);

		return sfBuilder.scanPackages(modelPackage).buildSessionFactory();
	}

	@Bean
	@Autowired
	@DependsOn("sessionFactory")
	public PlatformTransactionManager transactionManager(
			SessionFactory sessionFactory) {
		return new HibernateTransactionManager(sessionFactory);
	}
}