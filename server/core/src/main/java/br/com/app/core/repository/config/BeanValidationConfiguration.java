package br.com.app.core.repository.config;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.validation.MessageInterpolator;

import org.hibernate.validator.messageinterpolation.ResourceBundleMessageInterpolator;
import org.hibernate.validator.spi.resourceloading.ResourceBundleLocator;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.MessageSourceResourceBundle;
import org.springframework.util.Assert;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

/**
 * Configuration for the Bean Validation Implementation. Many model classes have
 * custom validation implementations.
 * <p>
 * Also it is important to notice that in order to use the new hibernate
 * validator 5 we need to override its message resource lookup.
 * 
 */
@Configuration
public class BeanValidationConfiguration {

	@Bean
	public MethodValidationPostProcessor methodValidationPostProcessor() {
		return new MethodValidationPostProcessor();
	}

	@Bean(destroyMethod = "")
	public LocalValidatorFactoryBean localValidatorFactoryBean() {
		return new MyValidatorFactoryBean();
	}

	public static class MyValidatorFactoryBean extends
			LocalValidatorFactoryBean {

		private static class HibernateValidatorDelegate {

			public static MessageInterpolator buildMessageInterpolator(
					MessageSource messageSource) {
				return new ResourceBundleMessageInterpolator(
						new MessageSourceResourceBundleLocator(messageSource));
			}
		}

		@Override
		public void setValidationMessageSource(MessageSource messageSource) {
			setMessageInterpolator(HibernateValidatorDelegate.buildMessageInterpolator(messageSource));
		}

	}

	public static class MessageSourceResourceBundleLocator implements
			ResourceBundleLocator {

		private final MessageSource messageSource;

		public MessageSourceResourceBundleLocator(MessageSource messageSource) {
			Assert.notNull(messageSource, "MessageSource must not be null");
			this.messageSource = messageSource;
		}

		@Override
		public ResourceBundle getResourceBundle(Locale locale) {
			return new MessageSourceResourceBundle(this.messageSource, locale);
		}
	}
}
