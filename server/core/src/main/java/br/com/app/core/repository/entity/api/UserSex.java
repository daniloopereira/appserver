package br.com.app.core.repository.entity.api;

public enum UserSex {
	MALE("M"), FEMALE("F");
	
	private String message;
	
	private UserSex(String message){
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
	
}
