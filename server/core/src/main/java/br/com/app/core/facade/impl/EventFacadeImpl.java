package br.com.app.core.facade.impl;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.app.core.facade.api.EventFacade;
import br.com.app.core.repository.api.EventRepository;
import br.com.app.core.repository.entity.Event;
import br.com.app.core.repository.entity.User;
import br.com.app.core.repository.entity.api.EventStatus;
import br.com.app.core.repository.entity.api.EventVisibility;

@Component
public class EventFacadeImpl implements EventFacade{
	
	@Autowired
	private EventRepository eventRepository;

	@Override
	public Event create(Event event) {
		Event eventReturn = new Event();
		eventReturn.setId(eventRepository.create(event));
		
		return eventReturn;
	}

	@Override
	public void update(Event event) {
		eventRepository.update(event);
	}

	@Override
	public List<Event> findByName(String name) {
		
		return eventRepository.findByName(name);
	}

	@Override
	public List<Event> findByDate(DateTime date) {
		
		return eventRepository.findByDate(date);
	}

	@Override
	public List<Event> findByAddress(String address) {
		return eventRepository.findByAddress(address);
	}

	@Override
	public List<Event> findByDateAddress(String dateAddress) {
		return eventRepository.findByDateAddress(dateAddress);
	}

	@Override
	public List<Event> findByStatus(EventVisibility status) {
		return eventRepository.findByStatus(status);
	}

	@Override
	public List<User> findByUserStatus(Event event, EventStatus status) {
		return eventRepository.findByUserStatus(event, status);
	}

	@Override
	public List<Event> findAll() {
		return eventRepository.findAll();
	}

}
