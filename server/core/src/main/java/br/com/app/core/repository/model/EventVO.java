package br.com.app.core.repository.model;

import java.io.Serializable;
import java.util.List;

import org.joda.time.DateTime;

import br.com.app.core.repository.entity.User;
import br.com.app.core.repository.entity.api.EventVisibility;

public class EventVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long id;
	private User owner;
	private DateTime date;
	private String title;	
	private Long quantityUsers;	
	private String address;	
	private String meetingAddress;	
	private List<User> users;	
	private EventVisibility visibility;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public User getOwner() {
		return owner;
	}
	public void setOwner(User owner) {
		this.owner = owner;
	}
	public DateTime getDate() {
		return date;
	}
	public void setDate(DateTime date) {
		this.date = date;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Long getQuantityUsers() {
		return quantityUsers;
	}
	public void setQuantityUsers(Long quantityUsers) {
		this.quantityUsers = quantityUsers;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getMeetingAddress() {
		return meetingAddress;
	}
	public void setMeetingAddress(String meetingAddress) {
		this.meetingAddress = meetingAddress;
	}
	public List<User> getUsers() {
		return users;
	}
	public void setUsers(List<User> users) {
		this.users = users;
	}
	public EventVisibility getVisibility() {
		return visibility;
	}
	public void setVisibility(EventVisibility visibility) {
		this.visibility = visibility;
	}

}
