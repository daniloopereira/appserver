package br.com.app.core.repository.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.app.core.repository.api.EventRepository;
import br.com.app.core.repository.entity.Event;
import br.com.app.core.repository.entity.User;
import br.com.app.core.repository.entity.api.EventStatus;
import br.com.app.core.repository.entity.api.EventVisibility;

@Repository
public class EventRepositoryImpl extends GenericDAOImpl<Event, Long>
	implements EventRepository{

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<Event> findByName(String name) {
		return criteria().add(Restrictions.eq("title", name)).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<Event> findByDate(DateTime date) {
		return criteria().add(Restrictions.eq("date", date)).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<Event> findByAddress(String address) {
		return criteria().add(Restrictions.eq("address", address)).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<Event> findByDateAddress(String dateAddress) {
		return criteria().add(Restrictions.eq("dateAddress", dateAddress)).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<Event> findByStatus(EventVisibility status) {
		return criteria().add(Restrictions.eq("visibillity", status)).list();
	}

	@Override
	@Transactional(readOnly=true)
	public List<User> findByUserStatus(Event event, EventStatus status) {
		Event eventz = this.find(event.getId());
		List<User> users = new ArrayList<User>();
		for(User user : eventz.getUsers()){
			if(user.getEventStatus() == status){
				users.add(user);
			}
		}
		return users;
	}

}
