package br.com.app.core.repository.api;

import java.util.List;

import br.com.app.core.repository.entity.UserEnterprise;

public interface UserEnterpriseRepository extends GenericDAO<UserEnterprise, Long>{
	
	List<UserEnterprise> findByName(String name);
	
	List<UserEnterprise> findByAddress(String address);
	
	UserEnterprise findByPhone(String phone);
	
	UserEnterprise findByCNPJ(String cnpj);

}
