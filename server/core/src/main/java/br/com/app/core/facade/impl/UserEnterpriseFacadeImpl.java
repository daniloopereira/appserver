package br.com.app.core.facade.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.app.core.facade.api.UserEnterpriseFacade;
import br.com.app.core.repository.api.UserEnterpriseRepository;
import br.com.app.core.repository.entity.UserEnterprise;

@Component
public class UserEnterpriseFacadeImpl implements UserEnterpriseFacade{
	
	@Autowired
	private UserEnterpriseRepository userEnterpriseRepo;

	@Override
	public List<UserEnterprise> findAll() {
		return userEnterpriseRepo.findAll();
	}

	@Override
	public void create(UserEnterprise userEnterprise) {
		userEnterpriseRepo.create(userEnterprise);
	}

	@Override
	public UserEnterprise update(UserEnterprise userEnterprise) {
		return userEnterpriseRepo.update(userEnterprise);
	}

	@Override
	public List<UserEnterprise> findByName(String name) {
		return userEnterpriseRepo.findByName(name);
	}

	@Override
	public UserEnterprise findByPhone(String phone) {
		return userEnterpriseRepo.findByPhone(phone);
	}

	@Override
	public UserEnterprise findByCnpj(String cnpj) {
		return userEnterpriseRepo.findByCNPJ(cnpj);
	}

}
