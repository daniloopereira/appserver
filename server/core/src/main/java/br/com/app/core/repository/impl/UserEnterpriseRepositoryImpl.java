package br.com.app.core.repository.impl;

import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.com.app.core.repository.api.UserEnterpriseRepository;
import br.com.app.core.repository.entity.UserEnterprise;

@Repository
public class UserEnterpriseRepositoryImpl extends GenericDAOImpl<UserEnterprise, Long>
	implements UserEnterpriseRepository{

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<UserEnterprise> findByName(String name) {
		return criteria().add(Restrictions.eq("name", name)).list();
	}

	@Override
	@Transactional(readOnly=true)
	public UserEnterprise findByPhone(String phone) {
		return (UserEnterprise) criteria().add(Restrictions.eq("phone", phone)).uniqueResult();
	}

	@Override
	@Transactional(readOnly=true)
	public UserEnterprise findByCNPJ(String cnpj) {
		return (UserEnterprise) criteria().add(Restrictions.eq("cnpj", cnpj)).uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional(readOnly=true)
	public List<UserEnterprise> findByAddress(String address) {
		return criteria().add(Restrictions.eq(address, address)).list();
	}
	
	

}
