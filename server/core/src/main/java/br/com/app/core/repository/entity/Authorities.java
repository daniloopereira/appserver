package br.com.app.core.repository.entity;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.springframework.security.core.GrantedAuthority;

import br.com.app.core.repository.entity.api.Roles;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(name = "ix_auth_user", columnNames = {
		"auth_id","authority" }) })
public class Authorities implements GrantedAuthority{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Enumerated(EnumType.STRING)
	private Roles authority;
	
	public Authorities(Roles authority) {
		this.authority = authority;
	}
	
	public Authorities(String authority) {
		this.authority = Roles.valueOf(authority);
	}
	
	public Authorities() {
	}

	@Override
	public String getAuthority() {
		return authority.name();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setAuthority(Roles authority) {
		this.authority = authority;
	}

}
