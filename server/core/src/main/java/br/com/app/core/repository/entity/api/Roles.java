package br.com.app.core.repository.entity.api;

public enum Roles {

	ROLE_ADMIN("server"), ROLE_MOBILE("mobile");

	private String desc;

	private Roles(String desc) {
		this.desc = desc;
	}

	public String getDesc() {
		return desc;
	}

	public String getName() {
		return name();
	}

}
