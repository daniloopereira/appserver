package br.com.app.core.repository.entity.api;

public enum EventStatus {
	
	YES("yes"), NO("no"), MAYBE("maybe");
	
	private String message;
	
	private EventStatus(String message){
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}

}
