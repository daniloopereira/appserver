package br.com.app.core.repository.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Example;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import br.com.app.core.repository.api.GenericDAO;
import br.com.app.core.repository.entity.api.Auditable;

@SuppressWarnings("unchecked")
public abstract class GenericDAOImpl<T extends Serializable, K extends Serializable>
		implements GenericDAO<T, K> {

	@Autowired
	protected SessionFactory sessionFactory;

	private final Class<T> type;

	/**
	 * This constructor calls the method getClassOfTypeParameter() to
	 * instantiate the class of the given parameterized type. It is up to any
	 * subclass to decide if it is needed to override the default
	 * implementation.
	 */
	public GenericDAOImpl() {
		type = getClassOfTypeParameter();
	}

	protected Session session() {
		return sessionFactory.getCurrentSession();
	}

	protected Criteria criteria() {
		return session().createCriteria(type);
	}

	/**
	 * Uses the GenericSuperclass approach to get the class object of the class
	 * first Type parameter.
	 * 
	 * @return the class of type T
	 */
	protected Class<T> getClassOfTypeParameter() {
		final Type t = getClass().getGenericSuperclass();
		final ParameterizedType pt = (ParameterizedType) t;
		return (Class<T>) pt.getActualTypeArguments()[0];
	}

	@Override
	@Transactional(readOnly = true)
	public Collection<T> byExample(T example) {

		return session().createCriteria(type)
				.add(Example.create(example).excludeZeroes().enableLike())
				.list();
	}

	@Override
	@Transactional(readOnly = true)
	public T byExampleUnique(T example) {

		final Collection<T> results = this.byExample(example);

//		Preconditions.checkState(results.size() < 2, "Non unique example");

		return results.size() == 1 ? results.iterator().next() : null;
	}

	@Override
	@Transactional
	public K create(final T t) {
		return (K) session().save(t);
	}

	@Override
	@Transactional
	public void delete(final T target) {
		session().delete(target);
	}

	@Override
	@Transactional(readOnly = true)
	public T find(final K id) {
		return (T) session().get(type, id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<T> findAll() {
		return session().createCriteria(type).list();
	}

	@Override
	@Transactional
	public T update(final T t) {

		if (t instanceof Auditable)
			((Auditable) t).setUpdatedDate(new DateTime());

		session().update(t);
		return t;
	}

	@Transactional
	public T merge(final T t) {
		session().merge(t);
		return t;
	}
}