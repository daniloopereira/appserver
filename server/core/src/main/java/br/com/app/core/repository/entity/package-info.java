@org.hibernate.annotations.TypeDefs({ @org.hibernate.annotations.TypeDef(name = "dateTime", defaultForType = DateTime.class, typeClass = PersistentDateTime.class) })
package br.com.app.core.repository.entity;

import org.jadira.usertype.dateandtime.joda.PersistentDateTime;
import org.joda.time.DateTime;

