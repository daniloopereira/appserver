CREATE TABLE `ClientAuth` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

CREATE TABLE `Authorities` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `authority` varchar(255) DEFAULT NULL,
  `auth_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ix_auth_user` (`auth_id`,`authority`),
  CONSTRAINT `FK_fiff82x0t3sc8ipg4gfo580rh` FOREIGN KEY (`auth_id`) REFERENCES `ClientAuth` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

INSERT IGNORE INTO ClientAuth (`id`, `name`, `password`, `username`)
VALUES (1,'server', '1234', 'server');

INSERT IGNORE INTO ClientAuth (`id`, `name`, `password`, `username`)
VALUES (2,'mobile', '1234', 'mobile');

INSERT IGNORE INTO `Authorities` (`id`, `authority`, `auth_id`)
VALUES (1, 'ROLE_ADMIN', 1);

INSERT IGNORE INTO `Authorities` (`id`, `authority`, `auth_id`)
VALUES (2, 'MOBILE', 2);
